const express = require("express");
const fs = require("fs");
const path = require("path");
const app = express();
const port = process.env.PORT || 3000;

const requestHandler = (req, res, next) => {
  const date = new Date().toString();
  const method = req.method;
  const url = req.url;
  const logMessage = `${date} ${method} ${url} \n`;
  fs.appendFile(path.join(__dirname, "request.log"), logMessage, (err) => {
    if (err) {
      console.error("Error writing to log file:", err);
    }
  });
  next();
};

app.use(requestHandler);

app.get("/home", (req, res) => {
  res.send("welcome to homepage");
});

app.get("/about", (req, res) => {
  res.send("About page");
});

app.get("/logs", (req, res) => {
  fs.readFile(path.join(__dirname, "request.log"), "utf8", (err, data) => {
    if (err) {
      res.status(500).send("Error reading log file");
    } else {
      res.send(data);
    }
  });
});

app.listen(port, () => {
  console.log("Server is running on port 3000");
});
